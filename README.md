```bash
//descargar proyecto
git clone https://gitlab.com/erika.castiblanco/bikes.git

//ver estado de archivos locales
git status

// agregar archivo para subir
// agrega todo
git add .
// agregar dinamicamente
git add -i
//volver a ver estado de archivos locales
git status
//crear commit (el commit es como el paquete de archivos que se desea subir osea los que estan en verde)
git commit -m 'aqui van sus comen tarios de porquè se van a subir los cambios'
// por ultimo se envian los cambios
git push
```

# Notas
```bash
Estudiante@T1406-PC20 MINGW64 /c/wamp64/www/Bikes (master)
$ git status
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)

        basededatos/
        controllers/
        vo/

nothing added to commit but untracked files present (use "git add" to track)

Estudiante@T1406-PC20 MINGW64 /c/wamp64/www/Bikes (master)
$ git add .

Estudiante@T1406-PC20 MINGW64 /c/wamp64/www/Bikes (master)
$ git status
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)

        new file:   basededatos/connection.php
        new file:   basededatos/login.php
        new file:   controllers/UserController.php
        new file:   vo/userClass.php


Estudiante@T1406-PC20 MINGW64 /c/wamp64/www/Bikes (master)
$ git commit -m 'commit init'

*** Please tell me who you are.

Run

  git config --global user.email "you@example.com"
  git config --global user.name "Your Name"

to set your account's default identity.
Omit --global to set the identity only in this repository.

fatal: unable to auto-detect email address (got 'Estudiante@T1406-PC20.(none)')

Estudiante@T1406-PC20 MINGW64 /c/wamp64/www/Bikes (master)
$ git config --global user.email "erika.castiblanco@colombogermana.edu.co"

Estudiante@T1406-PC20 MINGW64 /c/wamp64/www/Bikes (master)
$ git config --global user.name "Erika Castiblanco"

Estudiante@T1406-PC20 MINGW64 /c/wamp64/www/Bikes (master)
$ git commit -m 'commit init'
[master (root-commit) bbfe76f] commit init
 4 files changed, 28 insertions(+)
 create mode 100644 basededatos/connection.php
 create mode 100644 basededatos/login.php
 create mode 100644 controllers/UserController.php
 create mode 100644 vo/userClass.php

Estudiante@T1406-PC20 MINGW64 /c/wamp64/www/Bikes (master)
$ git push
remote: HTTP Basic: Access denied
fatal: Authentication failed for 'https://gitlab.com/erika.castiblanco/bikes.git/'

Estudiante@T1406-PC20 MINGW64 /c/wamp64/www/Bikes (master)
$ git push
Counting objects: 8, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (5/5), done.
Writing objects: 100% (8/8), 824 bytes | 117.00 KiB/s, done.
Total 8 (delta 0), reused 0 (delta 0)
To https://gitlab.com/erika.castiblanco/bikes.git
 * [new branch]      master -> master

Estudiante@T1406-PC20 MINGW64 /c/wamp64/www/Bikes (master)
$
```
